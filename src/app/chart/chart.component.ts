import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent implements OnInit {
  Stocks: any = [];

  public doughnutChartLabels: Label[] = ['GOOG', 'AMZN', 'C', 'AAPL', 'Others'];
  public doughnutChartData: MultiDataSet = [[]];
  public doughnutChartType: ChartType = "doughnut";
  public doughnutChartColors = [
    {
      backgroundColor: ['rgba(255,255,0,0.8)', 'rgba(0,255,0,0.5)', 'rgba(255,0,255,0.5)', 'rgba(0,0,255,0.7)', 'rgba(255,0,0,0.7)'],
    },
  ];

  public doughnutChartLegend = true;


  constructor(public restApi: RestApiService) { }
  ngOnInit(): void {
    this.loadChart();
  }

  loadChart() {
    return this.restApi.getStocks().subscribe((data: {}) => {
      this.Stocks = data;
      console.log("Length:", this.Stocks.length);
      var sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0, sum5 = 0;
      for (let i = 0; i < this.Stocks.length; i++) {
        console.log("in for loop");
        if (this.Stocks[i].stockTicker === "GOOG") {
          sum1 = sum1 + this.Stocks[i].volume;
        }
        else if (this.Stocks[i].stockTicker === "AMZN") {
          sum2 = sum2 + this.Stocks[i].volume;
        }
        else if (this.Stocks[i].stockTicker === "C") {
          sum3 = sum3 + this.Stocks[i].volume;
        }
        else if (this.Stocks[i].stockTicker === "AAPL") {
          sum4 = sum4 + this.Stocks[i].volume;
        }
        else {
          sum5 = sum5 + this.Stocks[i].volume;
        }
      }
      console.log(sum1, sum2, sum3, sum4, sum5);
      var tSum = sum1 + sum2 + sum3 + sum4 + sum5;
      var p1 = (sum1 * 100) / tSum;
      var p2 = (sum2 * 100) / tSum;
      var p3 = (sum3 * 100) / tSum;
      var p4 = (sum4 * 100) / tSum;
      var p5 = (sum5 * 100) / tSum;
      this.doughnutChartData = [[p1, p2, p3, p4, p5]];
      console.log(this.doughnutChartData);

    })
  }
}